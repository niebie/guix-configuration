;; init.scm -- default shepherd configuration file.

;; Services known to shepherd:
;; Add new services (defined using 'make <service>') to shepherd here by
;; providing them as arguments to 'register-services'.
(register-services
 (make <service>
   #:provides '(spoon)
   #:requires '()
   #:start (make-forkexec-constructor '("spoon")
                                      #:user "niebie"
                                      #:log-file "/home/niebie/logs/spoon.log")
   #:stop (make-kill-destructor))
 )

(register-services
 (make <service>
   #:provides '(fdm)
   #:requires '()
   #:start (make-system-constructor "~/sc/scripts/fdm-loop.sh")
   #:stop (make-kill-destructor)))

(register-services
 (make <service>
   #:provides '(emacs-daemon)
   #:requires '()
   #:start (make-system-constructor "emacs --daemon")
   #:stop (make-kill-destructor)))

(register-services
 (make <service>
   #:provides '(x-bg)
   #:start (make-system-constructor "xsetroot -solid grey")))

;; (register-services
;;  (make <service>
;;    #:provides '(fetchmail)
;;    #:requires '()
;;    #:start (make-system-constructor "fetchmail" " -v" " --ssl")
;;    #:stop (make-kill-destructor)))

;; Send shepherd into the background
(action 'shepherd 'daemonize)

;; Services to start when shepherd starts:
;; Add the name of each service that should be started to the list
;; below passed to 'for-each'.
(for-each start '(spoon emacs-daemon x-bg)) ;fdm
