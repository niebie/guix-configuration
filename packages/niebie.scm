;;; todo > license stuff

(define-module (niebie)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (gnu packages)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages image)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages base)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages ncurses)
  #:use-module ((guix licenses)
                #:select (gpl3))
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages protobuf))

(define* (github-uri user repo tag)
  "return github uri for source download"
  (string-append "https://github.com/"
                 user
                 "/"
                 repo
                 "/archive/"
                 tag
                 ".tar.gz"))

(define-public niebie-st
  (package
    (name "niebie-st")
    (version "master")
    (source
     (origin
       (method url-fetch)
       (uri (github-uri
             "niebieskitrociny"
             "st-fork"
             version))
       (sha256
        (base32
         "09kzkxya8v3wdnshg9szcnp8nq5078z3pjfxhf2ksp43yn99zczg"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list "CC=gcc"
                          (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-before 'build 'change-dir
           (lambda _
             (chdir "build")))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (zero?
                (system* "make" "install"
                         (string-append "DESTDIR=" out)
                         "PREFIX="
                         "CC=gcc"))))))))
    (inputs
     `(("libx11" ,libx11)
       ("libxft" ,libxft)
       ("fontconfig" ,fontconfig)
       ("freetype" ,freetype)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://github.com/niebieskitrociny/st-fork")
    (synopsis "small terminal")
    (description
     "niebie-st is a tiny terminal by suckless with minor changes by
niebieskitrociny.")
    (license license:x11)))

(define-public xst
  (package
    (name "xst")
    (version "0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/niebieskitrociny/xst/archive/master"
                           ".tar.gz"));; version ".tar.gz"))
       (sha256
        (base32
         "10zz78fvi6jcwgmk05c3s1yy7831195zsfgbfs3aqnqfg70s8pjj"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f ; no tests
       #:make-flags (list "CC=gcc"
                          (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'inhibit-terminfo-install
           (lambda _
             (substitute* "Makefile"
               (("\t@tic -s st.info") ""))
             #t)))))
    (inputs
     `(("libx11" ,libx11)
       ("libxft" ,libxft)
       ("fontconfig" ,fontconfig)
       ("freetype" ,freetype)
       ("libxext" ,libxext)
       ("ncurses" ,ncurses)))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page "http://st.suckless.org/")
    (synopsis "XResources Simple terminal emulator")
    (description
     "xst implements a simple and lightweight terminal emulator that complies
with the standard XResources system.  It implements 256 colors, most VT10X
escape sequences, utf8, X11 copy/paste, antialiased fonts (using fontconfig),
fallback fonts, resizing, and line drawing.")
    (license license:x11)))

(define-public niebie-dwm
  (package
    (name "niebie-dwm")
    (version "0.1")
    (source (origin
              (method url-fetch)
              (uri (github-uri
                    "niebieskitrociny"	;user
                    "dwm-fork"			;repo
                    version				;version
                    ))
              ;; (uri (string-append "https://github.com/niebieskitrociny/dwm-fork/archive/master"
              ;;                     ".tar.gz")) ;version ".tar.gz"));
              ;; "0qj0fkd0r4pfqwh89i3gn4ljw7l1fiqndirmlla5jjm39aa6i5v0"
              (sha256
               (base32
                "1if2xw4jkfs0j10bha7ngndxylj16485a8zaq0jvx2rpkj6yfh1r"
                ))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list (string-append "FREETYPEINC=-I"
                                         (assoc-ref %build-inputs "freetype")
                                         "/include/freetype2"))
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda _
             (substitute* "Makefile" (("\\$\\{CC\\}") "gcc"))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (zero?
                (system* "make" "install"
                         (string-append "DESTDIR=" out) "PREFIX=")))))
         (add-after 'build 'install-xsession
           (lambda* (#:key outputs #:allow-other-keys)
             ;; Add a .desktop file to xsessions.
             (let* ((output (assoc-ref outputs "out"))
                    (xsessions (string-append output "/share/xsessions")))
               (mkdir-p xsessions)
               (with-output-to-file
                   (string-append xsessions "/dwm.desktop")
                 (lambda _
                   (format #t
                           "[Desktop Entry]~@
                     Name=niebie-dwm~@
                     Comment=Dynamic Window Manager~@
                     Exec=~a/bin/dwm~@
                     TryExec=~@*~a/bin/dwm~@
                     Icon=~@
                     Type=Application~%"
                           output)))
               #t))))))
    (inputs
     `(("libx11" ,libx11)
       ("libxft" ,libxft)
       ("fontconfig" ,fontconfig)
       ("freetype" ,freetype)
       ("libxext" ,libxext)
       ("libxinerama" ,libxinerama)))
    (home-page "http://dwm.suckless.org/")
    (synopsis "Dynamic window manager")
    (description
     "dwm is a dynamic window manager for X.  It manages windows in tiled,
monocle and floating layouts.  All of the layouts can be applied dynamically,
optimising the environment for the application in use and the task performed.")
    (license license:x11)))

;;; important to have after the first definition otherwise this wont compile
(define-public niebie-dwm-latest
  (package (inherit niebie-dwm)
           (name "niebie-dwm-latest")
           (version "master")
           (source (origin
                     (method url-fetch)
                     (uri (github-uri
                           "niebieskitrociny"	;user
                           "dwm-fork"			;repo
                           version				;version
                           ))
                     (sha256
                      (base32
                       "0n9c8aa08g1j92xdw348l81rmr6fav5rg0rcb0dcj0qnqd2zdw0a"
                       ))
                     ))
           (inputs
            `(,@(package-inputs niebie-dwm)))
           (description "latest version of niebie-dwm")))

;;;

(define-public niebie-spoon
  (package
    (name "niebie-spoon")
    (version "0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/niebieskitrociny/spoon-fork/archive/master"
                           ".tar.gz"))  ;version ".tar.gz"));
       (sha256
        (base32
         "1cc4amwrizhrjrpkk0b9sbbgkxnfjal4xib5ilfjqw0qaxckh8y0"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list "CC=gcc"
                          (string-append "PREFIX=" %output))))
    (inputs
     `(("libx11" ,libx11)
       ("libxkbfile" ,libxkbfile)
       ("alsa-lib" ,alsa-lib)
       ("libmpdclient" ,libmpdclient)))
    (home-page "https://github.com/niebieskitrociny/spoon-fork")
    (synopsis "Set dwm status")
    (description
     "Spoon can be used to set the dwm status.")
    (license license:isc)))

;; (define github-uri)

(define-public niebie-protobuf
  (package
    (name "niebie-protobuf")
    (version "3.1.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/google/protobuf/releases/download/v3.1.0/protobuf-python-" version ".tar.gz") )
              ;; (uri (github-uri
              ;;        "google"						;;user
              ;;        "protobuf"					;;repo
              ;;        (string-append "v" version)	;;version
              ;;        ))
              (sha256
               (base32
                "01albycdwd4015gx9xncibj915rkrzxj2v68b2p4yqd903yhph8b"))))
    (build-system gnu-build-system)
    (inputs `(("unzip" ,unzip)
              ("gcc" ,gcc)
              ("autoconf" ,autoconf)
              ("libtool" ,libtool)
              ("automake" ,automake)
              ("curl" ,curl)
              ;; ("make" ,make)
              ))
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'autogen
           (lambda _
             (zero? (system* "sh" "autogen.sh")))))))
    (home-page "https://github.com/google/protobuf")
    (synopsis "Data encoding for remote procedure calls (RPCs)")
    (description
     "Protocol Buffers are a way of encoding structured data in an efficient
yet extensible format.  Google uses Protocol Buffers for almost all of its
internal RPC protocols and file formats.")
    (license #f)))

(define-public niebie-python-protobuf
  (package
    (name "niebie-python-protobuf")
    (version "3.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (github-uri
             "google" ;;user
             "protobuf" ;; repo
             (string-append "v" version) ;; version
             ))
       ;; (uri (pypi-uri "protobuf" version))
       (sha256
        (base32
         "0k8kxc3lxrp7hmd1c3nb8pv9lj1xd6z9frj4n8dlk5z89d7k2apv"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after
             'unpack 'change-directory
           (lambda* _ (chdir "python") #t)))))
    (inputs
     `(("niebie-protobuf" ,niebie-protobuf)))
    (propagated-inputs
     `(("python-six" ,python-six)))
    (home-page "https://github.com/google/protobuf")
    (synopsis "Protocol buffers is a data interchange format")
    (description
     "Protocol buffers are a language-neutral, platform-neutral extensible
mechanism for serializing structured data.")
    (license #f)))

(define-public python-readlike
  (package
    (name "python-readlike")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "readlike" version))
       (sha256
        (base32
         "1ck65ycw51f4xnbh4sg9axgbl81q3azpzvd5f2nvrv2fla9m8r08"))))
    (build-system python-build-system)
    ;; (arguments
    ;;  `(#:phases
    ;;    (modify-phases %standard-phases
    ;;      ;; Disable failing test. Bug filed upstream:
    ;;      ;; https://github.com/wardi/urwid/issues/164
    ;;      ;; TODO: check again for python-urwid > 1.3.1 or python > 3.4.3.
    ;;      (add-after 'unpack 'disable-failing-test
    ;;       (lambda _
    ;;         (substitute* "urwid/tests/test_event_loops.py"
    ;;           (("test_remove_watch_file")
    ;;             "disable_remove_watch_file")))))))
    (home-page "http://jangler.info/code/readlike")
    (synopsis "readlike")
    (description
     "readlike")
    (license #f)))

(define-public python-reparser
  (package
    (name "python-reparser")
    (version "1.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ReParser" version))
       (sha256
        (base32
         "0nniqb69xr0fv7ydlmrr877wyyjb61nlayka7xr08vlxl9caz776"))))
    (build-system python-build-system)
    ;; (arguments
    ;;  `(#:phases
    ;;    (modify-phases %standard-phases
    ;;      ;; Disable failing test. Bug filed upstream:
    ;;      ;; https://github.com/wardi/urwid/issues/164
    ;;      ;; TODO: check again for python-urwid > 1.3.1 or python > 3.4.3.
    ;;      (add-after 'unpack 'disable-failing-test
    ;;       (lambda _
    ;;         (substitute* "urwid/tests/test_event_loops.py"
    ;;           (("test_remove_watch_file")
    ;;             "disable_remove_watch_file")))))))
    (home-page "https://github.com/xmikos/reparser")
    (synopsis "Simple regex-based lexer/parser for inline markup")
    (description
     "Simple regex-based lexer/parser for inline markup")
    (license #f)))

(define-public python-reparser
  (package
    (name "python-reparser")
    (version "1.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ReParser" version))
       (sha256
        (base32
         "0nniqb69xr0fv7ydlmrr877wyyjb61nlayka7xr08vlxl9caz776"))))
    (build-system python-build-system)
    ;; (arguments
    ;;  `(#:phases
    ;;    (modify-phases %standard-phases
    ;;      ;; Disable failing test. Bug filed upstream:
    ;;      ;; https://github.com/wardi/urwid/issues/164
    ;;      ;; TODO: check again for python-urwid > 1.3.1 or python > 3.4.3.
    ;;      (add-after 'unpack 'disable-failing-test
    ;;       (lambda _
    ;;         (substitute* "urwid/tests/test_event_loops.py"
    ;;           (("test_remove_watch_file")
    ;;             "disable_remove_watch_file")))))))
    (home-page "https://github.com/xmikos/reparser")
    (synopsis "Simple regex-based lexer/parser for inline markup")
    (description
     "Simple regex-based lexer/parser for inline markup")
    (license #f)))

(define-public python-aiohttp
  (package
    (name "python-aiohttp")
    (version "2.2.5")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "aiohttp" version))
       (sha256
        (base32
         "1g6kzkf5in0briyscwxsihgps833dq2ggcq6lfh1hq95ck8zsnxg"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    ;; (arguments
    ;;  `(#:phases
    ;;    (modify-phases %standard-phases
    ;;      ;; Disable failing test. Bug filed upstream:
    ;;      ;; https://github.com/wardi/urwid/issues/164
    ;;      ;; TODO: check again for python-urwid > 1.3.1 or python > 3.4.3.
    ;;      (add-after 'unpack 'disable-failing-test
    ;;       (lambda _
    ;;         (substitute* "urwid/tests/test_event_loops.py"
    ;;           (("test_remove_watch_file")
    ;;             "disable_remove_watch_file")))))))
    (propagated-inputs
     `(("python-coverage" ,python-coverage)
       ("python-pytest" ,python-pytest)
       ("python-orderedmultidict" ,python-orderedmultidict)))
    (home-page "https://github.com/aio-libs/aiohttp/")
    (synopsis "aiohttp")
    (description
     "aiohttp")
    (license #f)))

(define-public python-yarl
  (package
    (name "python-yarl")
    (version "0.12.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "yarl" version))
              ;; (uri (github-uri
              ;;        "bw2"
              ;;        "ConfigArgParse"
              ;;        version))
              (sha256
               (base32
                "1pnb28jw857p9p710b6c2jmmf9czinkb7c47nbab90p8vkzp23zw"))))
    (build-system python-build-system)
    (arguments
     ;; FIXME: Bug in test suite filed upstream:
     ;; https://github.com/bw2/ConfigArgParse/issues/32
     '(#:tests? #f))
    (synopsis "yarl")
    (description "yarl")
    (home-page "https://github.com/aio-libs/yarl")
    (license license:expat)))

(define-public niebie-python-configargparse
  (package
    (name "niebie-python-configargparse")
    (version "0.11.0")
    (source (origin
              (method url-fetch)
              (uri (github-uri
                    "bw2"
                    "ConfigArgParse"
                    version))
              (sha256
               (base32
                "0vmcbdmyfpvlhxpkzxal4lbgnqqzzch4fmg8mf73kp3sxv0l6a3z"))))
    (build-system python-build-system)
    (arguments
     ;; FIXME: Bug in test suite filed upstream:
     ;; https://github.com/bw2/ConfigArgParse/issues/32
     '(#:tests? #f))
    (synopsis "Replacement for argparse")
    (description "A drop-in replacement for argparse that allows options to also
be set via config files and/or environment variables.")
    (home-page "https://github.com/bw2/ConfigArgParse")
    (license license:expat)))

(define-public python-async_timeout
  (package
    (name "python-async_timeout")
    (version "1.4.0")
    (source (origin
              (method url-fetch)
              (uri (github-uri
                    "aio-libs"
                    "async_timeout"
                    (string-append "v" version)))
              ;; (uri (pypi-uri "async_timeout" version))
              (sha256
               (base32
                "08ajp1w7bhw332i01lhkmkdbsw1wvfa2frvxgsmxr9gjczpncwv2"))))
    (build-system python-build-system)
    (arguments
     ;; FIXME: Bug in test suite filed upstream:
     ;; https://github.com/bw2/ConfigArgParse/issues/32
     '(#:tests? #f))
    (synopsis "async_timeout")
    (description "async_timeout")
    (home-page "https://github.com/aio-libs/async_timeout")
    (license license:expat)))

(define-public python-multidict
  (package
    (name "python-multidict")
    (version "3.2.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "multidict" version))
              ;; (uri (github-uri
              ;;        "bw2"
              ;;        "ConfigArgParse"
              ;;        version))
              (sha256
               (base32
                "1clcp3xylv8pnmhkwb5v6yii0kqliip6y857rmgc2wbk66aplyp2"))))
    (build-system python-build-system)
    (arguments
     ;; FIXME: Bug in test suite filed upstream:
     ;; https://github.com/bw2/ConfigArgParse/issues/32
     '(#:tests? #f))
    (synopsis "multidict")
    (description "multidict")
    (home-page "https://github.com/aio-libs/multidict")
    (license license:expat)))

(define-public python-hangups
  (package
    (name "python-hangups")
    (version "0.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (github-uri
             "tdryer"					;user
             "hangups"				;repo
             (string-append "v" version))) ;tag
       (sha256
        (base32
         "1dybqby3gn0djvx9ig41g2y0jgwymz3iphiz36xw7k2ir25lp9gs"))))
    (arguments
     '(#:tests? #f))
    (propagated-inputs
     `(("python-MechanicalSoup" ,python-MechanicalSoup)
       ("python-urwid" ,python-urwid)
       ("niebie-python-protobuf" ,niebie-python-protobuf)
       ("python-reparser" ,python-reparser)
       ("python-requests" ,python-requests)
       ("python-readlike" ,python-readlike)
       ("python-appdirs" ,python-appdirs)
       ("python-aiohttp" ,python-aiohttp)
       ("niebie-python-configargparse" ,niebie-python-configargparse)
       ("python-beautifulsoup4" ,python-beautifulsoup4)
       ("python-yarl" ,python-yarl)
       ("python-async_timeout" ,python-async_timeout)
       ("python-multidict" ,python-multidict)
       ("python-chardet" ,python-chardet)))
    (build-system python-build-system)
    (home-page "https://github.com/tdryer/hangups")
    (synopsis "Hangouts protocol reverse engineer")
    (description "Provides a python interface to interact with google hangouts.")
    (license license:asl2.0)))

(define-public python-MechanicalSoup
  (package
    (name "python-MechanicalSoup")
    (version "0.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (github-uri
             "hickford"					;user
             "MechanicalSoup"				;repo
             (string-append "v" version))) ;tag
       (sha256
        (base32
         "0l68sh4f48c5mbrsd11vzllls13lparb12rkb9ym4avblnl00ywd"))))
    (build-system python-build-system)
    (inputs
     `(("python-six" ,python-six)
       ("python-beautifulsoup4" ,python-beautifulsoup4)
       ("python-requests" ,python-requests)))
    (home-page "https://github.com/hickford/MechanicalSoup")
    (synopsis "mech soup")
    (description "mech soup")
    (license #f)))

(define-public hangups_cli
  (package
    (name "hangups_cli")
    (version "0.0")
    (source
     (origin
       (method url-fetch)
       (uri (github-uri
             "jtamagnan"					;user
             "hangups_cli"				;repo
             "master")) ;tag
       ;; (uri (string-append "https://github.com/jtamagnan/hangups_cli/archive/master" ".tar.gz")) ;version "tar.gz"))
       (sha256
        (base32
         "1mcd6q1z938drxk4pvx6icvqhp9cb9iwp297p4iycspfj7mg1y2p"))	;change
       ))
    (build-system python-build-system)
    (propagated-inputs
     `(("python-argcomplete" ,python-argcomplete)
       ("python-hangups" ,python-hangups)))
    (home-page "https://github.com/jtamagnan/hangups_cli")
    (synopsis "An interface to the hangouts reverse engineering effort")
    (description "This tool is used to build rich interfaces to the hangouts messaging service.")
    (license gpl3)))
