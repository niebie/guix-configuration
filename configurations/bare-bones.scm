(use-modules (gnu))
(use-service-modules networking ssh)
(use-package-modules admin)

(operating-system
  (host-name "hyperion")
  (timezone "America/Chicago")
  (locale "en_US.utf8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root" is
  ;; the label of the target root file system.
  (bootloader (grub-configuration (device "/dev/sda")))
  (file-systems (cons (file-system
                        (device "guix")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  ;; This is where user accounts are specified.  The "root"
  ;; account is implicit, and is initially created with the
  ;; empty password.
  (users (cons (user-account
                (name "niebie")
                (comment "Tom Balzer")
                (group "users")

                ;; Adding the account to the "wheel" group
                ;; makes it a sudoer.  Adding it to "audio"
                ;; and "video" allows the user to play sound
                ;; and access the webcam.
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"))
                (home-directory "/home/niebie"))
               %base-user-accounts))

  ;; Globally-installed packages.
  (packages %base-packages)

  ;; Add services to the baseline: a DHCP client and
  ;; an SSH server.
  (services (cons* %base-services))
)
