(use-modules (gnu) (gnu system nss))
(use-package-modules certs ;; version-control
                     suckless
                     xdisorg)
(use-service-modules xorg dbus)
(use-modules (guix packages) (guix git-download))

(define %artwork-repository
  (let ((commit "6998d30425289b087c64f63e7415df2241e591db"))
    (origin
      (method git-fetch)
      (uri (git-reference
             (url "git://git.savannah.gnu.org/guix/guix-artwork.git")
             (commit commit)))
      (file-name (string-append "guix-artwork-" (string-take commit 7)
                                "-checkout"))
      (sha256
       (base32
        "0k7j3pj9s3zqiqmfkapypssvzx3f12yr0cc2rbzxqfii0b4clp1j")))))

(define %default-slim-theme
  ;; Theme based on work by Felipe López.
  (file-append %artwork-repository "/slim"))

(define %default-slim-theme-name
  ;; This must be the name of the sub-directory in %DEFAULT-SLIM-THEME that
  ;; contains the actual theme files.
  "0.x")

(operating-system
  (host-name "niebie-guix")
  (timezone "America/Chicago")
  (locale "en_US.UTF-8")
  (bootloader (grub-configuration (target "/dev/sda")))
  (file-systems (cons (file-system
                        (device "guix-root")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (users (cons (user-account
                (name "niebie")
                (comment "niebieskitrociny")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"
                                        "tty"))
                (home-directory "/home/niebie"))
               %base-user-accounts))
  (packages
   (append
    (map (compose list specification->package+output)
         '("niebie-dwm-latest")
         )
    (cons* nss-certs         ;for HTTPS access
           ;; gvfs              ;for user mounts
           %base-packages)))
                                        ;window manager
  (services (cons*
             (slim-service
              ;; #:allow-empty-passwords? #f
              ;; #:theme %default-slim-theme
              ;; #:theme-name %default-slim-theme-name
              )
             (dbus-service)
             (screen-locker-service slock)
             (screen-locker-service xlockmore "xlock")
                                        ;gets rid of awful errors on tty1
             ;; %desktop-services
             %base-services
             ))

  ;; (services ;; (cons* ;; (postgresql-service)
  ;;        ;;         ;; (service cgit-service-type
  ;;        ;;         ;;           (cgit-configuration
  ;;        ;;         ;;            (nginx %cgit-configuration-nginx)))
  ;;        ;;         ;; (service openssh-service-type
  ;;        ;;         ;;       (openssh-configuration
  ;;        ;;         ;;        (x11-forwarding? #t)
  ;;        ;;         ;;        (permit-root-login #t)))
  ;;        ;;         %desktop-services)
  ;;        )
  (name-service-switch %mdns-host-lookup-nss))





;;; taken from wigust's dotfiles
;; (define %cgit-configuration-nginx
;;   (list
;;    (nginx-server-configuration
;;     (root cgit)
;;     (locations
;;      (list
;;       (nginx-location-configuration
;;        (uri "@cgit")
;;        (body '("fastcgi_param SCRIPT_FILENAME $document_root/lib/cgit/cgit.cgi;"
;;                "fastcgi_param PATH_INFO $uri;"
;;                "fastcgi_param QUERY_STRING $args;"
;;                "fastcgi_param HTTP_HOST $server_name;"
;;                "fastcgi_pass 127.0.0.1:9000;")))))
;;     ;; (try-files (list "$uri" "@cgit"))
;;     (http-port 19418)
;;     (https-port #f)
;;     (ssl-certificate #f)
;;      (ssl-certificate-key #f))))
