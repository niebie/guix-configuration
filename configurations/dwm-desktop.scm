(use-modules (gnu) (gnu system nss))
(use-modules (gnu services desktop))
(use-modules (gnu services databases))
(use-service-modules web version-control)
(use-package-modules certs gnome version-control)


;;; taken from wigust's dotfiles
;; (define %cgit-configuration-nginx
;;   (list
;;    (nginx-server-configuration
;;     (root cgit)
;;     (locations
;;      (list
;;       (nginx-location-configuration
;;        (uri "@cgit")
;;        (body '("fastcgi_param SCRIPT_FILENAME $document_root/lib/cgit/cgit.cgi;"
;;                "fastcgi_param PATH_INFO $uri;"
;;                "fastcgi_param QUERY_STRING $args;"
;;                "fastcgi_param HTTP_HOST $server_name;"
;;                "fastcgi_pass 127.0.0.1:9000;")))))
;;     ;; (try-files (list "$uri" "@cgit"))
;;     (http-port 19418)
;;     (https-port #f)
;;     (ssl-certificate #f)
;;      (ssl-certificate-key #f))))


(operating-system
  (host-name "niebie-guix")
  (timezone "America/Chicago")
  (locale "en_US.UTF-8")
  (bootloader (grub-configuration (target "/dev/sda")))
  (file-systems (cons (file-system
                        (device "guix-root")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (users (cons (user-account
                (name "niebie")
                (comment "niebieskitrociny")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"
                                        "tty"))
                (home-directory "/home/niebie"))
               %base-user-accounts))
  (packages
   (append
    (map (compose list specification->package+output)
         '("dwm")
         )

    (cons* nss-certs         ;for HTTPS access
           gvfs              ;for user mounts
           %base-packages)))             ;window manager

  (services (cons* ;; (postgresql-service)
             ;; (service cgit-service-type
             ;;             (cgit-configuration
             ;;              (nginx %cgit-configuration-nginx)))
             %desktop-services))
  (name-service-switch %mdns-host-lookup-nss))
