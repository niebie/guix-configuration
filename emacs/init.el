(require 'package) ;; You might already have this line
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

;; todo> fix for fucking make files. (hooks prob)
(setq-default indent-tabs-mode nil)

;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(inhibit-startup-screen t)
;;  '(menu-bar-mode nil)
;;  '(scroll-bar-mode nil)
;;  '(tool-bar-mode nil))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )

;; (emms-all)
;; (emms-default-players)
;; (setq emms-source-file-default-directory "~/music/")

;; (require 'web-mode)
;; (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

;; (add-to-list 'load-path "~/.emacs.d/sc/mastodon.el/lisp")
;; (require 'mastodon)

(require 'key-chord)
(key-chord-mode 1)

(setq key-chord-two-keys-delay 0.05)

;; disabled, don't have ripgrep installed
;;(key-chord-define-global "rg"     'ripgrep-regexp)
(key-chord-define-global "rg"     'rgrep)
(key-chord-define-global "lf"     'load-file)
(key-chord-define-global "rs"     'replace-string)
(key-chord-define-global "st"     'todo-show)
(key-chord-define-global "gm"     'god-mode)

;; (require 'package) ;; You might already have this line
;; (let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
;;              (not (gnutls-available-p))))
;;        (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
;;   (add-to-list 'package-archives (cons "melpa" url) t)
;;   (add-to-list 'package-archives
;;             '("marmalade" . "http://marmalade-repo.org/packages/")))
;; (when (< emacs-major-version 24)
;;   ;; For important compatibility libraries like cl-lib
;;   (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
;; (package-initialize) ;; You might already have this line

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(column-number-mode t)
 '(compilation-message-face (quote default))
 '(custom-enabled-themes (quote (Default-Console)))
 '(custom-safe-themes
   (quote
    ("34ed3e2fa4a1cb2ce7400c7f1a6c8f12931d8021435bad841fdc1192bd1cc7da" "760ce657e710a77bcf6df51d97e51aae2ee7db1fba21bbad07aab0fa0f42f834" "e1498b2416922aa561076edc5c9b0ad7b34d8ff849f335c13364c8f4276904f0" "1263771faf6967879c3ab8b577c6c31020222ac6d3bac31f331a74275385a452" "73ad471d5ae9355a7fa28675014ae45a0589c14492f52c32a4e9b393fcc333fd" "aea30125ef2e48831f46695418677b9d676c3babf43959c8e978c0ad672a7329" "36746ad57649893434c443567cb3831828df33232a7790d232df6f5908263692" "3380a2766cf0590d50d6366c5a91e976bdc3c413df963a0ab9952314b4577299" "77a46326228485699b378a8537f9bc5d6b0d087566ac179bec752fab322d814a" "7bef2d39bac784626f1635bd83693fae091f04ccac6b362e0405abf16a32230c" "25c06a000382b6239999582dfa2b81cc0649f3897b394a75ad5a670329600b45" "c158c2a9f1c5fcf27598d313eec9f9dceadf131ccd10abc6448004b14984767c" "36282815a2eaab9ba67d7653cf23b1a4e230e4907c7f110eebf3cdf1445d8370" "0ee3fc6d2e0fc8715ff59aed2432510d98f7e76fe81d183a0eb96789f4d897ca" "d6922c974e8a78378eacb01414183ce32bc8dbf2de78aabcc6ad8172547cb074" "89531935dc9c4620a06579e525d5365f94639cb4c5c328bcabbd22bc39a862ed" "1b1e54d9e0b607010937d697556cd5ea66ec9c01e555bb7acea776471da59055" "47744f6c8133824bdd104acc4280dbed4b34b85faa05ac2600f716b0226fb3f6" "6ee6f99dc6219b65f67e04149c79ea316ca4bcd769a9e904030d38908fd7ccf9" "2d16f85f22f1841390dfc1234bd5acfcce202d9bb1512aa8eabd0068051ac8c3" "365d9553de0e0d658af60cff7b8f891ca185a2d7ba3fc6d29aadba69f5194c7f" "b81bfd85aed18e4341dbf4d461ed42d75ec78820a60ce86730fc17fc949389b2" "77ab33a45a8d39566f24cd6a9b28c78eec89882004ed194587e210399813d009" "b378249b7f647796b186c70f61eaaee7aa1bd123681d5ca8c44d3ca8875e1b70" "611e38c2deae6dcda8c5ac9dd903a356c5de5b62477469133c89b2785eb7a14d" "6f11ad991da959fa8de046f7f8271b22d3a97ee7b6eca62c81d5a917790a45d9" "ff7625ad8aa2615eae96d6b4469fcc7d3d20b2e1ebc63b761a349bebbb9d23cb" "e1994cf306356e4358af96735930e73eadbaf95349db14db6d9539923b225565" "ade8f08b7bc75347519f386afbe0c802ff3ca657072abae6c872fbb7c8b51742" "555c5a7fa39f8d1538501cc3fdb4fba7562ec4507f1665079021870e0a4c57d8" "3e8ea6a37f17fd9e0828dee76b7ba709319c4d93b7b21742684fadd918e8aca3" "ce22122f56e2ca653678a4aaea2d1ea3b1e92825b5ae3a69b5a2723da082b8a4" "66881e95c0eda61d34aa7f08ebacf03319d37fe202d68ecf6a1dbfd49d664bc3" "4af6fad34321a1ce23d8ab3486c662de122e8c6c1de97baed3aa4c10fe55e060" "4d80487632a0a5a72737a7fc690f1f30266668211b17ba836602a8da890c2118" "417a047001847a55f9e0d9692f2bde644a325ab8a1ef18b22baea8309d9164cb" "2968a2ef2d6053ef935d6d537b32a94c6bec868706dc937e1049473db9e60645" "429028d30a6cc349351417616112c9d9346282ee951c874988199c4a28f952f5" "3eb2b5607b41ad8a6da75fe04d5f92a46d1b9a95a202e3f5369e2cdefb7aac5c" "17a9f83e580932aa26411b04357dc887441a1cace15f8b90a23b2c09d1ce17e6" "b3bcf1b12ef2a7606c7697d71b934ca0bdd495d52f901e73ce008c4c9825a3aa" "de3a1d5ff021ba9389412649691aaa11febcd144f41999d359d41c3384d21e63" "55d31108a7dc4a268a1432cd60a7558824223684afecefa6fae327212c40f8d3" default)))
 '(display-battery-mode t)
 '(fci-rule-color "#383838")
 '(fringe-mode 10 nil (fringe))
 '(highlight-changes-colors (quote ("#ff8eff" "#ab7eff")))
 '(highlight-tail-colors
   (quote
    (("#424748" . 0)
     ("#63de5d" . 20)
     ("#4BBEAE" . 30)
     ("#1DB4D0" . 50)
     ("#9A8F21" . 60)
     ("#A75B00" . 70)
     ("#F309DF" . 85)
     ("#424748" . 100))))
 '(inhibit-startup-screen t)
 '(jdee-db-active-breakpoint-face-colors (cons "#000000" "#fd971f"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#000000" "#b6e63e"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#000000" "#525254"))
 '(linum-format " %6d ")
 '(magit-diff-use-overlays nil)
 '(main-line-color1 "#222232")
 '(main-line-color2 "#333343")
 '(mastodon-instance-url "https://linuxrocks.online")
 '(menu-bar-mode nil)
 '(mu4e-bookmarks
   (quote
    (("flag:unread AND NOT flag:trashed" "Unread messages" 117)
     ("date:today..now AND NOT flag:trashed" "Today's messages, no trash" 116)
     ("date:7d..now AND NOT flag:trashed" "Last 7 days, no trash" 119)
     ("mime:image/*" "Messages with images" 112)
     ("from:masaaki" "mizuno bookmark" 109))))
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(org-ellipsis "  ")
 '(org-fontify-done-headline t)
 '(org-fontify-quote-and-verse-blocks t)
 '(org-fontify-whole-heading-line t)
 '(package-selected-packages
   (quote
    (geiser key-chord god-mode emms rainbow-blocks restclient nginx-mode web-mode debbugs plantuml-mode bui adaptive-wrap pdf-tools rainbow-delimiters paredit magit gruvbox-theme greymatters-theme green-phosphor-theme gotham-theme gandalf-theme forest-blue-theme faff-theme eziam-theme exotica-theme eclipse-theme dracula-theme doom-themes django-theme darkokai-theme darkburn-theme dark-mint-theme danneskjold-theme cyberpunk-theme creamsody-theme clues-theme bubbleberry-theme boron-theme bliss-theme basic-theme base16-theme autumn-light-theme)))
 '(plantuml-jar-path "/home/niebie/.guix-profile/share/java")
 '(pos-tip-background-color "#E6DB74")
 '(pos-tip-foreground-color "#242728")
 '(powerline-color1 "#222232")
 '(powerline-color2 "#333343")
 '(safe-local-variable-values
   (quote
    ((eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")
     (bug-reference-bug-regexp . "<https?://\\(debbugs\\|bugs\\)\\.gnu\\.org/\\([0-9]+\\)>"))))
 '(scroll-bar-mode nil)
 '(send-mail-function (quote sendmail-send-it))
 '(sendmail-program
   "/gnu/store/nj4hxxvly7zi5wv2qmlkrr515sk248w6-sendmail-8.15.2/usr/sbin/sendmail")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 465)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(user-full-name "Tom Balzer")
 '(user-mail-address "niebieskitrociny@gmail.com")
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3")
 '(weechat-color-list
   (unspecified "#242728" "#424748" "#F70057" "#ff0066" "#86C30D" "#63de5d" "#BEB244" "#E6DB74" "#40CAE4" "#06d8ff" "#FF61FF" "#ff8eff" "#00b2ac" "#53f2dc" "#f8fbfc" "#ffffff")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Courier"))))
 '(font-lock-comment-delimiter-face ((t (:foreground "green"))))
 '(font-lock-comment-face ((t (:foreground "forest green"))))
 '(magit-diff-added ((t (:background "#335522" :foreground "white" :weight bold))))
 '(magit-diff-added-highlight ((t (:background "#336600" :foreground "#ca9900" :weight bold))))
 '(magit-diff-removed ((t (:background "#553333" :foreground "#ffdddd" :weight bold))))
 '(magit-diff-removed-highlight ((t (:background "#663333" :foreground "#ccffff" :weight extra-bold))))
 '(match ((t (:background "#000fff" :foreground "#ffffff" :weight bold)))))

(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'geiser-repl-mode-hook #'enable-paredit-mode)

;; (setq message-alternative-emails
;;       (regexp-opt '("niebieskitrociny@gmail.com")))

;; (setq mu4e-update-interval 300)

;; (setq load-path (cons ('load-path) "/gnu/store/9r713mhicrl3csawcw2g47p7h25wxrk4-mu-0.9.18/share/emacs/site-lisp/"))

;; (require 'web-mode)
;; (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
