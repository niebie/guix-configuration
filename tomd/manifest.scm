(add-to-load-path "/home/niebie/sc/tomd/guile")

(define-module (tomd manifest)
  #:use-module (tomd job)
  #:export (job-list))

(define launch-spoon
  (create-job #:name "spoon"
              #:command-line "/home/niebie/sc/spoon-fork/spoon"
              ; this is meant to kill all things matching the regex when booting
              ;#:pgrep-string "^spoon$"
              ))

(define emacs-daemon
  (create-job #:name "emacs-daemon"
              #:command-line "emacs"
              #:args "--daemon"
              ;;#:shutdown-cmd "emacsclient"
              ;;#:shutdown-args (list "-e")
              ))

(define xsetroot-bg
  (create-job #:name "xsetroot"
              #:command-line "xsetroot"
              #:args (list "-solid" "grey")))

(define job-list (list launch-spoon
                       xsetroot-bg
                       ;;emacs-daemon
                       ))
