;;; this is a manifest that has a development environment for sdl2.

(use-modules (gnu) (gnu system))

(use-package-modules emacs ;duh
                     version-control ;git
                     suckless ;st
                     compression ;unzip
                     sdl
                     gcc
                     package-management ;guix
                     commencement ;gcc-toolchain
                     )

(use-modules (niebie ;niebie-spoon
              ))

(use-package-modules linux
                     less
                     zile
                     nano
                     admin
                     lsof
                     pciutils
                     man
                     texinfo
                     bash
                     guile
                     gawk
                     base)

(packages->manifest
 (list guix ; needed so that guix is still usable after search-path'ing.
       emacs
       emacs-guix
       git
       (list git "gui")
       st
       unzip
       sdl2
       gcc
       binutils
       gcc-toolchain
       gnu-make

       procps psmisc which less zile nano
       lsof                                 ;for Guix's 'list-runtime-roots'
       pciutils usbutils
       util-linux inetutils isc-dhcp
       (@ (gnu packages admin) shadow)          ;for 'passwd'

       ;; wireless-tools is deprecated in favor of iw, but it's still what
       ;; many people are familiar with, so keep it around.
       iw wireless-tools rfkill

       iproute
       net-tools                        ; XXX: remove when Inetutils suffices
       man-db
       info-reader                     ;the standalone Info reader (no Perl)

       ;; The 'sudo' command is already in %SETUID-PROGRAMS, but we also
       ;; want the other commands and the man pages (notably because
       ;; auto-completion in Emacs shell relies on man pages.)
       sudo

       ;; Get 'insmod' & co. from kmod, not module-init-tools, since udev
       ;; already depends on it anyway.
       kmod eudev

       e2fsprogs kbd

       bash-completion

       ;; XXX: We don't use (canonical-package guile-2.2) here because that
       ;; would create a collision in the global profile between the GMP
       ;; variant propagated by 'guile-final' and the GMP variant propagated
       ;; by 'gnutls', itself propagated by 'guix'.
       guile-2.2
       bash
       coreutils
       findutils
       grep
       sed
       diffutils
       patch
       gawk
       tar
       gzip
       bzip2
       xz
       lzip))
