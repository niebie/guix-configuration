(manifest
  (version 3)
  (packages
    (("graphviz"
      "2.40.1"
      "out"
      "/gnu/store/aqza2bb1sdcqrzqg3xf1xrmw8qs6zjp0-graphviz-2.40.1"
      (propagated-inputs ())
      (search-paths ()))
     ("niebie-dwm-latest"
      "master"
      "out"
      "/gnu/store/hngvyhqsk8ijmn2j7ssha8wdz04x71cv-niebie-dwm-latest-master"
      (propagated-inputs ())
      (search-paths ()))
     ("emacs"
      "25.3"
      "out"
      "/gnu/store/6cflji7h6y0v15dvnccv7paaa7894gdc-emacs-25.3"
      (propagated-inputs ())
      (search-paths
        (("INFOPATH" ("share/info") ":" directory #f))))
     ("emacs-yasnippet"
      "0.12.2"
      "out"
      "/gnu/store/qh4vb1khpl77s13sgdvsiqv7f1wgncqw-emacs-yasnippet-0.12.2"
      (propagated-inputs ())
      (search-paths ()))
     ("emacs-go-mode"
      "1.5.0"
      "out"
      "/gnu/store/cb4l5m1kq5xd20vjhbi4gyviglryzgxg-emacs-go-mode-1.5.0"
      (propagated-inputs ())
      (search-paths ()))
     ("emacs-guix"
      "0.3.4"
      "out"
      "/gnu/store/hd15z5vx8k28jwx1axqxhjp0a5vvk47a-emacs-guix-0.3.4"
      (propagated-inputs
        (("geiser"
          "0.9"
          "out"
          "/gnu/store/fdcsywdi5730j8q4afsm4bgjfxmg2pw3-geiser-0.9"
          (propagated-inputs ())
          (search-paths ()))
         ("emacs-dash"
          "2.13.0"
          "out"
          "/gnu/store/l3y1bkv4jhw7hgi552cafc0c70r2nk2m-emacs-dash-2.13.0"
          (propagated-inputs ())
          (search-paths ()))
         ("emacs-bui"
          "1.1.0"
          "out"
          "/gnu/store/9i05sb9rjai8jh1qz1wgk3yhl1q1nyc7-emacs-bui-1.1.0"
          (propagated-inputs
            (("emacs-dash"
              "2.13.0"
              "out"
              "/gnu/store/l3y1bkv4jhw7hgi552cafc0c70r2nk2m-emacs-dash-2.13.0"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))
         ("emacs-magit-popup"
          "2.12.0"
          "out"
          "/gnu/store/c820ys935s19x5nyf0g8aicalq9d3mzm-emacs-magit-popup-2.12.0"
          (propagated-inputs
            (("emacs-dash"
              "2.13.0"
              "out"
              "/gnu/store/l3y1bkv4jhw7hgi552cafc0c70r2nk2m-emacs-dash-2.13.0"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))))
      (search-paths ()))
     ("dvtm"
      "0.15"
      "out"
      "/gnu/store/mlihcf4yimgwfjwh5ml0na9ll4wvn8j1-dvtm-0.15"
      (propagated-inputs ())
      (search-paths ()))
     ("swig"
      "3.0.12"
      "out"
      "/gnu/store/qhr0f37bn259bn1vrdjsf5mjnk77876x-swig-3.0.12"
      (propagated-inputs ())
      (search-paths ()))
     ("libtool"
      "2.4.6"
      "out"
      "/gnu/store/jqaa2fyahi7m83idz9f06pk9gkgf8cn8-libtool-2.4.6"
      (propagated-inputs
        (("m4"
          "1.4.18"
          "out"
          "/gnu/store/r89snxr0r5zv60rmj0rgx2vv45h3sywn-m4-1.4.18"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths ()))
     ("guile-opengl"
      "0.1.0"
      "out"
      "/gnu/store/yvzmzvjs5yy4f7rbhnpq7kn0aim6hxyz-guile-opengl-0.1.0"
      (propagated-inputs ())
      (search-paths ()))
     ("guile"
      "2.2.3"
      "out"
      "/gnu/store/gwspk20b7fbrs4l5rzgaadf8896h12bq-guile-2.2.3"
      (propagated-inputs
        (("libunistring"
          "0.9.7"
          "out"
          "/gnu/store/lrmv2nbv8498ny39ig5jny5av6x59vw3-libunistring-0.9.7"
          (propagated-inputs ())
          (search-paths ()))
         ("libltdl"
          "2.4.6"
          "out"
          "/gnu/store/r76ybspyhb7hd8abz7lmhyifk0cfbqd5-libltdl-2.4.6"
          (propagated-inputs ())
          (search-paths ()))
         ("libgc"
          "7.6.0"
          "out"
          "/gnu/store/izagp1i0rvmi8bvn6yrgrgkc6vs09s15-libgc-7.6.0"
          (propagated-inputs ())
          (search-paths ()))
         ("gmp"
          "6.1.2"
          "out"
          "/gnu/store/wy9a7wv9lmmmlly6r26vw6x0dz9z83s7-gmp-6.1.2"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths
        (("GUILE_LOAD_PATH"
          ("share/guile/site/2.2")
          ":"
          directory
          #f)
         ("GUILE_LOAD_COMPILED_PATH"
          ("lib/guile/2.2/site-ccache"
           "share/guile/site/2.2")
          ":"
          directory
          #f))))
     ("sendmail"
      "8.15.2"
      "out"
      "/gnu/store/alnnwjrvxlflqd9f52ida4cak2v191i4-sendmail-8.15.2"
      (propagated-inputs ())
      (search-paths ()))
     ("fdm"
      "1.9"
      "out"
      "/gnu/store/ip21sp80pm66kk8j6xw215vcyjsh78qh-fdm-1.9"
      (propagated-inputs ())
      (search-paths ()))
     ("automake"
      "1.15.1"
      "out"
      "/gnu/store/gda08c59fhyc81s8ddklndznlfjcvrr1-automake-1.15.1"
      (propagated-inputs ())
      (search-paths
        (("ACLOCAL_PATH"
          ("share/aclocal")
          ":"
          directory
          #f))))
     ("autobuild"
      "5.3"
      "out"
      "/gnu/store/ikf45kbq18yv77ca3i50b92pni9g56fl-autobuild-5.3"
      (propagated-inputs ())
      (search-paths ()))
     ("autoconf"
      "2.69"
      "out"
      "/gnu/store/sv9yr2yzzy0n3cnhg8f7njb4166l04zj-autoconf-2.69"
      (propagated-inputs ())
      (search-paths ()))
     ("guile-sdl2"
      "0.2.0"
      "out"
      "/gnu/store/rmripa21jl9paiwkf171agf5a0s7ka0k-guile-sdl2-0.2.0"
      (propagated-inputs ())
      (search-paths ()))
     ("libffi"
      "3.2.1"
      "out"
      "/gnu/store/a9zmlfh0rlwhz8znks8rl2qw63h3mhlc-libffi-3.2.1"
      (propagated-inputs ())
      (search-paths ()))
     ("zip"
      "3.0"
      "out"
      "/gnu/store/fwsahsdzinbhhlngja88c17600qf46m0-zip-3.0"
      (propagated-inputs ())
      (search-paths ()))
     ("nix"
      "1.11.9"
      "out"
      "/gnu/store/5fhancfy0bi28b2c712c0pdf3j9b1pmd-nix-1.11.9"
      (propagated-inputs ())
      (search-paths ()))
     ("git"
      "2.16.1"
      "gui"
      "/gnu/store/js36z6q52iy5jdb3293rlwlc99r7dk0r-git-2.16.1-gui"
      (propagated-inputs ())
      (search-paths
        (("GIT_SSL_CAINFO"
          ("etc/ssl/certs/ca-certificates.crt")
          #f
          regular
          #f)
         ("GIT_EXEC_PATH"
          ("libexec/git-core")
          #f
          directory
          #f))))
     ("mailutils"
      "3.4"
      "out"
      "/gnu/store/0y41i22y0v0gkgibrqpbrn86n6z9biym-mailutils-3.4"
      (propagated-inputs ())
      (search-paths ()))
     ("guile-bytestructures"
      "1.0.1"
      "out"
      "/gnu/store/9vhg6dfkxxcm7w6z1sknalhlmv8b8q0n-guile-bytestructures-1.0.1"
      (propagated-inputs ())
      (search-paths ()))
     ("xinput"
      "1.6.2"
      "out"
      "/gnu/store/w8py2gibcphwgf7c57s3km011n2p367a-xinput-1.6.2"
      (propagated-inputs ())
      (search-paths ()))
     ("go"
      "1.9.2"
      "out"
      "/gnu/store/a6fsi3mg70qaisypvlry6kj3qgqd8a5v-go-1.9.2"
      (propagated-inputs ())
      (search-paths ()))
     ("adb"
      "7.1.2_r6"
      "out"
      "/gnu/store/c4znm5cxj7956mb2hhw1zyp7giw491lv-adb-7.1.2_r6"
      (propagated-inputs ())
      (search-paths ()))
     ("obs"
      "18.0.2"
      "out"
      "/gnu/store/bvmckpq1dbrjpxw9vvnnhmwxr5ln3s1y-obs-18.0.2"
      (propagated-inputs ())
      (search-paths ()))
     ("surf"
      "2.0"
      "out"
      "/gnu/store/5c1bbgj138s91y93bpcr387rb5gry0j5-surf-2.0"
      (propagated-inputs ())
      (search-paths ()))
     ("xrandr"
      "1.5.0"
      "out"
      "/gnu/store/4f41jcnjpydvm96sbab0y3k3lmhgaakn-xrandr-1.5.0"
      (propagated-inputs ())
      (search-paths ()))
     ("vlc"
      "2.2.6"
      "out"
      "/gnu/store/az9ry0bxgcw3svi20sn3ywd4gv8jq43g-vlc-2.2.6"
      (propagated-inputs ())
      (search-paths ()))
     ("icedtea"
      "3.5.1"
      "out"
      "/gnu/store/ylf77pbsa66k0vp9f6fzlgrp7fvr5ikp-icedtea-3.5.1"
      (propagated-inputs ())
      (search-paths ()))
     ("plantuml"
      "8048"
      "out"
      "/gnu/store/dggx2dasqlrszgr9yf4b44snrpidff0n-plantuml-8048"
      (propagated-inputs ())
      (search-paths ()))
     ("font-gnu-freefont-ttf"
      "20120503"
      "out"
      "/gnu/store/xk818r92wh1drykyc0h941qzjls2k54k-font-gnu-freefont-ttf-20120503"
      (propagated-inputs ())
      (search-paths ()))
     ("cgit"
      "1.1"
      "out"
      "/gnu/store/b4zg0sz6z1n32qnwmgmbbwh78i55hr56-cgit-1.1"
      (propagated-inputs ())
      (search-paths ()))
     ("youtube-dl-gui"
      "0.3.8"
      "out"
      "/gnu/store/zrxpqb81zm8wl4l526b0p2f2z0ka76hn-youtube-dl-gui-0.3.8"
      (propagated-inputs ())
      (search-paths ()))
     ("guile2.0-git"
      "0.0-3.e156a10"
      "out"
      "/gnu/store/i3vwxy7854k50njk1c3b3i60i9kmc6rl-guile2.0-git-0.0-3.e156a10"
      (propagated-inputs
        (("guile2.0-bytestructures"
          "20170402.91d042e"
          "out"
          "/gnu/store/ywbn3bfpb9av0krhr3b69y8pnhr1aw22-guile2.0-bytestructures-20170402.91d042e"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths ()))
     ("libxft"
      "2.3.2"
      "out"
      "/gnu/store/1510kbj4hzxq3dn03v3017g6gzvflyr6-libxft-2.3.2"
      (propagated-inputs
        (("libxrender"
          "0.9.10"
          "out"
          "/gnu/store/fnrzxhihrxjai3a9d3rw5h3z613iiqk2-libxrender-0.9.10"
          (propagated-inputs
            (("renderproto"
              "0.11.1"
              "out"
              "/gnu/store/0rdrki3avpahfj3h2ghx6g2s7aph7m6n-renderproto-0.11.1"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))
         ("freetype"
          "2.7.1"
          "out"
          "/gnu/store/2sq8w3x8glbjlfn22im6nwwycmbdlzws-freetype-2.7.1"
          (propagated-inputs
            (("libpng"
              "1.6.28"
              "out"
              "/gnu/store/88wvqp60hbrdvbp0xsqad5c6njjfshcw-libpng-1.6.28"
              (propagated-inputs
                (("zlib"
                  "1.2.11"
                  "out"
                  "/gnu/store/yd7bplsvf9nj72wn2z6n38rq9hfmjgd9-zlib-1.2.11"
                  (propagated-inputs ())
                  (search-paths ()))))
              (search-paths ()))
             ("zlib"
              "1.2.11"
              "out"
              "/gnu/store/yd7bplsvf9nj72wn2z6n38rq9hfmjgd9-zlib-1.2.11"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))
         ("fontconfig"
          "2.12.1"
          "out"
          "/gnu/store/zqr2k8lp2cpkry5lbgndjjxxs791ki6v-fontconfig-2.12.1"
          (propagated-inputs
            (("expat"
              "2.2.0"
              "out"
              "/gnu/store/w3k48r6bi511zvi8ykx1jj1hlp5n09ji-expat-2.2.2"
              (propagated-inputs ())
              (search-paths ()))
             ("freetype"
              "2.7.1"
              "out"
              "/gnu/store/2sq8w3x8glbjlfn22im6nwwycmbdlzws-freetype-2.7.1"
              (propagated-inputs
                (("libpng"
                  "1.6.28"
                  "out"
                  "/gnu/store/88wvqp60hbrdvbp0xsqad5c6njjfshcw-libpng-1.6.28"
                  (propagated-inputs
                    (("zlib"
                      "1.2.11"
                      "out"
                      "/gnu/store/yd7bplsvf9nj72wn2z6n38rq9hfmjgd9-zlib-1.2.11"
                      (propagated-inputs ())
                      (search-paths ()))))
                  (search-paths ()))
                 ("zlib"
                  "1.2.11"
                  "out"
                  "/gnu/store/yd7bplsvf9nj72wn2z6n38rq9hfmjgd9-zlib-1.2.11"
                  (propagated-inputs ())
                  (search-paths ()))))
              (search-paths ()))))
          (search-paths ()))))
      (search-paths ()))
     ("libx11"
      "1.6.5"
      "out"
      "/gnu/store/xb4pyf01jakxajm2f41qmhsma9pvvwv2-libx11-1.6.5"
      (propagated-inputs
        (("kbproto"
          "1.0.7"
          "out"
          "/gnu/store/vx6908dnzjcpdgr0kw278cngjsswxjvs-kbproto-1.0.7"
          (propagated-inputs ())
          (search-paths ()))
         ("libxcb"
          "1.12"
          "out"
          "/gnu/store/wx603r6ks2c6bxjyk2mbkrs0l5d8xh8q-libxcb-1.12"
          (propagated-inputs
            (("libpthread-stubs"
              "0.3"
              "out"
              "/gnu/store/ja2yd1xs1frhcx29ah6rrvrwmcd9aypq-libpthread-stubs-0.3"
              (propagated-inputs ())
              (search-paths ()))
             ("libxau"
              "1.0.8"
              "out"
              "/gnu/store/y4mvzf7aa9c8a2byjq1v49w0r1zp1hxn-libxau-1.0.8"
              (propagated-inputs
                (("xproto"
                  "7.0.31"
                  "out"
                  "/gnu/store/c4fa4s83rk7y538m78nv2fg3x99js3vs-xproto-7.0.31"
                  (propagated-inputs
                    (("util-macros"
                      "1.19.0"
                      "out"
                      "/gnu/store/95r80wxavrhc947iy2rkqq6fs2dr9g3q-util-macros-1.19.0"
                      (propagated-inputs ())
                      (search-paths ()))))
                  (search-paths ()))))
              (search-paths ()))
             ("libxdmcp"
              "1.1.2"
              "out"
              "/gnu/store/il5pqp6j4rcx005ji96aqqmqw9s37l5s-libxdmcp-1.1.2"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))))
      (search-paths ()))
     ("xorg-server"
      "1.19.3"
      "out"
      "/gnu/store/g64xa8wb8xl16hgdhzc5p1a4lcqg206y-xorg-server-1.19.3"
      (propagated-inputs
        (("dri2proto"
          "2.8"
          "out"
          "/gnu/store/iy59ynn1zqdlkdkakgm1h2cl8gdsq074-dri2proto-2.8"
          (propagated-inputs ())
          (search-paths ()))
         ("dri3proto"
          "1.0"
          "out"
          "/gnu/store/l480isyaq039yj8c9yrxx6wls181pmf3-dri3proto-1.0"
          (propagated-inputs ())
          (search-paths ()))
         ("fontsproto"
          "2.1.3"
          "out"
          "/gnu/store/gpambppys1n0iraqcwmd3pmrfnc8qpgk-fontsproto-2.1.3"
          (propagated-inputs ())
          (search-paths ()))
         ("inputproto"
          "2.3.2"
          "out"
          "/gnu/store/m6f23fqb1i8yxsbrp3krk62b3mbd5vcl-inputproto-2.3.2"
          (propagated-inputs ())
          (search-paths ()))
         ("kbproto"
          "1.0.7"
          "out"
          "/gnu/store/vx6908dnzjcpdgr0kw278cngjsswxjvs-kbproto-1.0.7"
          (propagated-inputs ())
          (search-paths ()))
         ("libpciaccess"
          "0.13.5"
          "out"
          "/gnu/store/9sgscigfvrf6wklirgnmhnjqhajljhbv-libpciaccess-0.13.5"
          (propagated-inputs ())
          (search-paths ()))
         ("mesa"
          "17.0.6"
          "out"
          "/gnu/store/p9rv7a71z7klykjg24f0js8ahn2yjpqr-mesa-17.0.6"
          (propagated-inputs
            (("glproto"
              "1.4.17"
              "out"
              "/gnu/store/v8sazfqz0rw9r97ikz5a6s57chc73qmc-glproto-1.4.17"
              (propagated-inputs ())
              (search-paths ()))
             ("libdrm"
              "2.4.80"
              "out"
              "/gnu/store/6lhl3il1lx8swj6m6zd9pmb1z06l7w8c-libdrm-2.4.80"
              (propagated-inputs ())
              (search-paths ()))
             ("libvdpau"
              "1.1.1"
              "out"
              "/gnu/store/1q6va15h5n1rjhh48p834imah666napv-libvdpau-1.1.1"
              (propagated-inputs ())
              (search-paths ()))
             ("libx11"
              "1.6.5"
              "out"
              "/gnu/store/xb4pyf01jakxajm2f41qmhsma9pvvwv2-libx11-1.6.5"
              (propagated-inputs
                (("kbproto"
                  "1.0.7"
                  "out"
                  "/gnu/store/vx6908dnzjcpdgr0kw278cngjsswxjvs-kbproto-1.0.7"
                  (propagated-inputs ())
                  (search-paths ()))
                 ("libxcb"
                  "1.12"
                  "out"
                  "/gnu/store/wx603r6ks2c6bxjyk2mbkrs0l5d8xh8q-libxcb-1.12"
                  (propagated-inputs
                    (("libpthread-stubs"
                      "0.3"
                      "out"
                      "/gnu/store/ja2yd1xs1frhcx29ah6rrvrwmcd9aypq-libpthread-stubs-0.3"
                      (propagated-inputs ())
                      (search-paths ()))
                     ("libxau"
                      "1.0.8"
                      "out"
                      "/gnu/store/y4mvzf7aa9c8a2byjq1v49w0r1zp1hxn-libxau-1.0.8"
                      (propagated-inputs
                        (("xproto"
                          "7.0.31"
                          "out"
                          "/gnu/store/c4fa4s83rk7y538m78nv2fg3x99js3vs-xproto-7.0.31"
                          (propagated-inputs
                            (("util-macros"
                              "1.19.0"
                              "out"
                              "/gnu/store/95r80wxavrhc947iy2rkqq6fs2dr9g3q-util-macros-1.19.0"
                              (propagated-inputs ())
                              (search-paths ()))))
                          (search-paths ()))))
                      (search-paths ()))
                     ("libxdmcp"
                      "1.1.2"
                      "out"
                      "/gnu/store/il5pqp6j4rcx005ji96aqqmqw9s37l5s-libxdmcp-1.1.2"
                      (propagated-inputs ())
                      (search-paths ()))))
                  (search-paths ()))))
              (search-paths ()))
             ("libxdamage"
              "1.1.4"
              "out"
              "/gnu/store/dn1dngmysb8bp9l360cw2mi1qin2mb4q-libxdamage-1.1.4"
              (propagated-inputs
                (("damageproto"
                  "1.2.1"
                  "out"
                  "/gnu/store/v0a3rwi3aqxw6dm2x7n5jl5nnrxp31x5-damageproto-1.2.1"
                  (propagated-inputs ())
                  (search-paths ()))
                 ("libxfixes"
                  "5.0.3"
                  "out"
                  "/gnu/store/vcfqjvmyfplgcl2csfvm1yp1i47lxmbc-libxfixes-5.0.3"
                  (propagated-inputs
                    (("fixesproto"
                      "5.0"
                      "out"
                      "/gnu/store/lfk4hnc3fid0c3i3g1mwx5p0iky6b5dc-fixesproto-5.0"
                      (propagated-inputs
                        (("xextproto"
                          "7.3.0"
                          "out"
                          "/gnu/store/kzsc03i5lhj2giklhdg8lkpg9iz484mg-xextproto-7.3.0"
                          (propagated-inputs ())
                          (search-paths ()))))
                      (search-paths ()))))
                  (search-paths ()))
                 ("xproto"
                  "7.0.31"
                  "out"
                  "/gnu/store/c4fa4s83rk7y538m78nv2fg3x99js3vs-xproto-7.0.31"
                  (propagated-inputs
                    (("util-macros"
                      "1.19.0"
                      "out"
                      "/gnu/store/95r80wxavrhc947iy2rkqq6fs2dr9g3q-util-macros-1.19.0"
                      (propagated-inputs ())
                      (search-paths ()))))
                  (search-paths ()))
                 ("libx11"
                  "1.6.5"
                  "out"
                  "/gnu/store/xb4pyf01jakxajm2f41qmhsma9pvvwv2-libx11-1.6.5"
                  (propagated-inputs
                    (("kbproto"
                      "1.0.7"
                      "out"
                      "/gnu/store/vx6908dnzjcpdgr0kw278cngjsswxjvs-kbproto-1.0.7"
                      (propagated-inputs ())
                      (search-paths ()))
                     ("libxcb"
                      "1.12"
                      "out"
                      "/gnu/store/wx603r6ks2c6bxjyk2mbkrs0l5d8xh8q-libxcb-1.12"
                      (propagated-inputs
                        (("libpthread-stubs"
                          "0.3"
                          "out"
                          "/gnu/store/ja2yd1xs1frhcx29ah6rrvrwmcd9aypq-libpthread-stubs-0.3"
                          (propagated-inputs ())
                          (search-paths ()))
                         ("libxau"
                          "1.0.8"
                          "out"
                          "/gnu/store/y4mvzf7aa9c8a2byjq1v49w0r1zp1hxn-libxau-1.0.8"
                          (propagated-inputs
                            (("xproto"
                              "7.0.31"
                              "out"
                              "/gnu/store/c4fa4s83rk7y538m78nv2fg3x99js3vs-xproto-7.0.31"
                              (propagated-inputs
                                (("util-macros"
                                  "1.19.0"
                                  "out"
                                  "/gnu/store/95r80wxavrhc947iy2rkqq6fs2dr9g3q-util-macros-1.19.0"
                                  (propagated-inputs ())
                                  (search-paths ()))))
                              (search-paths ()))))
                          (search-paths ()))
                         ("libxdmcp"
                          "1.1.2"
                          "out"
                          "/gnu/store/il5pqp6j4rcx005ji96aqqmqw9s37l5s-libxdmcp-1.1.2"
                          (propagated-inputs ())
                          (search-paths ()))))
                      (search-paths ()))))
                  (search-paths ()))))
              (search-paths ()))
             ("libxfixes"
              "5.0.3"
              "out"
              "/gnu/store/vcfqjvmyfplgcl2csfvm1yp1i47lxmbc-libxfixes-5.0.3"
              (propagated-inputs
                (("fixesproto"
                  "5.0"
                  "out"
                  "/gnu/store/lfk4hnc3fid0c3i3g1mwx5p0iky6b5dc-fixesproto-5.0"
                  (propagated-inputs
                    (("xextproto"
                      "7.3.0"
                      "out"
                      "/gnu/store/kzsc03i5lhj2giklhdg8lkpg9iz484mg-xextproto-7.3.0"
                      (propagated-inputs ())
                      (search-paths ()))))
                  (search-paths ()))))
              (search-paths ()))
             ("libxshmfence"
              "1.2"
              "out"
              "/gnu/store/g7idqq2sjhwas5xg4x054qx4i7jycvmw-libxshmfence-1.2"
              (propagated-inputs ())
              (search-paths ()))
             ("libxxf86vm"
              "1.1.4"
              "out"
              "/gnu/store/7v3ff6lfkl8cahaw3s3k6h6060fchy8n-libxxf86vm-1.1.4"
              (propagated-inputs
                (("libxext"
                  "1.3.3"
                  "out"
                  "/gnu/store/z932xghmgjyya5qbikilk6vgw6wg6qrx-libxext-1.3.3"
                  (propagated-inputs
                    (("xextproto"
                      "7.3.0"
                      "out"
                      "/gnu/store/kzsc03i5lhj2giklhdg8lkpg9iz484mg-xextproto-7.3.0"
                      (propagated-inputs ())
                      (search-paths ()))))
                  (search-paths ()))
                 ("xf86vidmodeproto"
                  "2.3.1"
                  "out"
                  "/gnu/store/n6lk1a4v9x0yxvvzw2hl665xlprr4hfl-xf86vidmodeproto-2.3.1"
                  (propagated-inputs ())
                  (search-paths ()))))
              (search-paths ()))))
          (search-paths ()))
         ("pixman"
          "0.34.0"
          "out"
          "/gnu/store/z3r8kvwfbnz8i9d7i9km7x3gbdzdsyqg-pixman-0.34.0"
          (propagated-inputs ())
          (search-paths ()))
         ("presentproto"
          "1.0"
          "out"
          "/gnu/store/55rnz9n9zv58agxlnb4670vnyl9rwg8k-presentproto-1.0"
          (propagated-inputs ())
          (search-paths ()))
         ("randrproto"
          "1.5.0"
          "out"
          "/gnu/store/8al1qkkdrgfnrw5pzv92ksdpnhl4qvyh-randrproto-1.5.0"
          (propagated-inputs ())
          (search-paths ()))
         ("renderproto"
          "0.11.1"
          "out"
          "/gnu/store/0rdrki3avpahfj3h2ghx6g2s7aph7m6n-renderproto-0.11.1"
          (propagated-inputs ())
          (search-paths ()))
         ("resourceproto"
          "1.2.0"
          "out"
          "/gnu/store/x5hqgb0mixfjddvkbhd0v2hwpp0nppif-resourceproto-1.2.0"
          (propagated-inputs ())
          (search-paths ()))
         ("scrnsaverproto"
          "1.2.2"
          "out"
          "/gnu/store/d9vvbsbv9bsfahmidkc59fpz59g0lhfg-scrnsaverproto-1.2.2"
          (propagated-inputs ())
          (search-paths ()))
         ("videoproto"
          "2.3.3"
          "out"
          "/gnu/store/am6wm32dbvaxdk0dyq95z2if54629kxf-videoproto-2.3.3"
          (propagated-inputs ())
          (search-paths ()))
         ("xextproto"
          "7.3.0"
          "out"
          "/gnu/store/kzsc03i5lhj2giklhdg8lkpg9iz484mg-xextproto-7.3.0"
          (propagated-inputs ())
          (search-paths ()))
         ("xineramaproto"
          "1.2.1"
          "out"
          "/gnu/store/qqbxgclw1gm6f6bn1b9nbq3idm4d6yjb-xineramaproto-1.2.1"
          (propagated-inputs ())
          (search-paths ()))
         ("xf86driproto"
          "2.1.1"
          "out"
          "/gnu/store/5qdr9vj6n3cafkg6bfmrgrx4jg7xcgvy-xf86driproto-2.1.1"
          (propagated-inputs ())
          (search-paths ()))
         ("xproto"
          "7.0.31"
          "out"
          "/gnu/store/c4fa4s83rk7y538m78nv2fg3x99js3vs-xproto-7.0.31"
          (propagated-inputs
            (("util-macros"
              "1.19.0"
              "out"
              "/gnu/store/95r80wxavrhc947iy2rkqq6fs2dr9g3q-util-macros-1.19.0"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))))
      (search-paths ()))
     ("xinit"
      "1.3.4"
      "out"
      "/gnu/store/dcgm0nihgmdvpvx4savdc7afxnsdmirr-xinit-1.3.4"
      (propagated-inputs
        (("xauth"
          "1.0.10"
          "out"
          "/gnu/store/s5699pw6ygrk2cqb6zdmplzc2wg566qi-xauth-1.0.10"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths ()))
     ("freetype"
      "2.7.1"
      "out"
      "/gnu/store/2sq8w3x8glbjlfn22im6nwwycmbdlzws-freetype-2.7.1"
      (propagated-inputs
        (("libpng"
          "1.6.28"
          "out"
          "/gnu/store/88wvqp60hbrdvbp0xsqad5c6njjfshcw-libpng-1.6.28"
          (propagated-inputs
            (("zlib"
              "1.2.11"
              "out"
              "/gnu/store/yd7bplsvf9nj72wn2z6n38rq9hfmjgd9-zlib-1.2.11"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths ()))
         ("zlib"
          "1.2.11"
          "out"
          "/gnu/store/yd7bplsvf9nj72wn2z6n38rq9hfmjgd9-zlib-1.2.11"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths ()))
     ("vala"
      "0.36.3"
      "out"
      "/gnu/store/i7hp51psmy5jmfpysfq2q62jfsi08gnm-vala-0.36.3"
      (propagated-inputs
        (("glib"
          "2.52.2"
          "out"
          "/gnu/store/6fm1vhmddnlc0a4ma4k5pnabz233d09q-glib-2.52.2"
          (propagated-inputs
            (("pcre"
              "8.40"
              "out"
              "/gnu/store/4ddshxi7fnddwakfvgjq3jg3jjbb3syh-pcre-8.41"
              (propagated-inputs ())
              (search-paths ()))))
          (search-paths
            (("XDG_DATA_DIRS" ("share") ":" directory #f)
             ("GIO_EXTRA_MODULES"
              ("lib/gio/modules")
              ":"
              directory
              #f))))))
      (search-paths
        (("XDG_DATA_DIRS" ("share") ":" directory #f)
         ("GIO_EXTRA_MODULES"
          ("lib/gio/modules")
          ":"
          directory
          #f))))
     ("glib"
      "2.52.2"
      "out"
      "/gnu/store/6fm1vhmddnlc0a4ma4k5pnabz233d09q-glib-2.52.2"
      (propagated-inputs
        (("pcre"
          "8.40"
          "out"
          "/gnu/store/4ddshxi7fnddwakfvgjq3jg3jjbb3syh-pcre-8.41"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths
        (("XDG_DATA_DIRS" ("share") ":" directory #f)
         ("GIO_EXTRA_MODULES"
          ("lib/gio/modules")
          ":"
          directory
          #f))))
     ("pulseaudio"
      "10.0"
      "out"
      "/gnu/store/32n1j9zgn8xxgfsi4svnm4yrichirfcp-pulseaudio-10.0"
      (propagated-inputs
        (("libcap"
          "2.25"
          "out"
          "/gnu/store/g5c83pcizib7q89w8c9qn8v0cjba6c7p-libcap-2.25"
          (propagated-inputs ())
          (search-paths ()))
         ("gdbm"
          "1.12"
          "out"
          "/gnu/store/rhg8yvnxip0ixppbi50smwbpb28ra2s9-gdbm-1.12"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths ()))
     ("glibc"
      "2.25"
      "out"
      "/gnu/store/qszb1ha50yv333ssghag1kb881qqarv8-glibc-2.25"
      (propagated-inputs
        (("linux-libre-headers"
          "4.4.47"
          "out"
          "/gnu/store/yw4nhrk2hwd5yac1ni0x49i2pd8a19br-linux-libre-headers-4.4.47"
          (propagated-inputs ())
          (search-paths ()))))
      (search-paths
        (("GUIX_LOCPATH" ("lib/locale") ":" directory #f))))
     ("xset"
      "1.2.3"
      "out"
      "/gnu/store/57dssjz18zfd10x633ny9xgnr4z0qmmx-xset-1.2.3"
      (propagated-inputs ())
      (search-paths ()))
     ("strace"
      "4.18"
      "out"
      "/gnu/store/8hgq082mb0smxs57cwixlli95ncarl90-strace-4.18"
      (propagated-inputs ())
      (search-paths ()))
     ("gdb"
      "8.0"
      "out"
      "/gnu/store/0lxhl93nfnkmkvd8nzdhadh0x47npm6w-gdb-8.0"
      (propagated-inputs ())
      (search-paths ()))
     ("dash"
      "0.5.9.1"
      "out"
      "/gnu/store/2rkm62p440qhhvbw437hysh19nw0wspy-dash-0.5.9.1"
      (propagated-inputs ())
      (search-paths ()))
     ("zsh"
      "5.2"
      "out"
      "/gnu/store/jx3qac4h92fxw2y9q86d7m84b69kwhj0-zsh-5.2"
      (propagated-inputs ())
      (search-paths ()))
     ("cmake"
      "3.7.2"
      "out"
      "/gnu/store/j8574k2xb98bs3a996hzpnl45m9q5axw-cmake-3.7.2"
      (propagated-inputs ())
      (search-paths
        (("CMAKE_PREFIX_PATH" ("") ":" directory #f))))
     ("htop"
      "2.0.2"
      "out"
      "/gnu/store/1j49pga72vsj911qm4c1q8cw8z7i8kd3-htop-2.0.2"
      (propagated-inputs ())
      (search-paths ()))
     ("alsa-utils"
      "1.1.4"
      "out"
      "/gnu/store/wh097a5wq5xgdir8xcj2fa1whlhmxz2d-alsa-utils-1.1.4"
      (propagated-inputs ())
      (search-paths ()))
     ("lsh"
      "2.1"
      "out"
      "/gnu/store/27402bimw4x797yp0h3l15mqpyjbvc6n-lsh-2.1"
      (propagated-inputs ())
      (search-paths ()))
     ("python"
      "3.5.3"
      "out"
      "/gnu/store/s31njsb8cplmly202gjsxk9165cyb672-python-3.5.3"
      (propagated-inputs ())
      (search-paths
        (("PYTHONPATH"
          ("lib/python3.5/site-packages")
          ":"
          directory
          #f))))
     ("chromium-bsu"
      "0.9.16.1"
      "out"
      "/gnu/store/a0ikbp8b0cgkslry85hl4v7w1nrvfgcn-chromium-bsu-0.9.16.1"
      (propagated-inputs ())
      (search-paths ()))
     ("icecat"
      "52.1.0-gnu1"
      "out"
      "/gnu/store/aqk7b0hsiqssxvjfh44vary8i9wlsmk7-icecat-52.1.0-gnu1"
      (propagated-inputs ())
      (search-paths ()))
     ("imagemagick"
      "6.9.9-9"
      "out"
      "/gnu/store/bsyp3smax35lw4nkx6398gsb4n3fszlx-imagemagick-6.9.9-9"
      (propagated-inputs ())
      (search-paths ()))
     ("gimp"
      "2.8.22"
      "out"
      "/gnu/store/rggc3j43cza16kpkl3kd6mrm4b2ac037-gimp-2.8.22"
      (propagated-inputs ())
      (search-paths ()))
     ("ntfs-3g"
      "2017.3.23"
      "out"
      "/gnu/store/xv5pnq390bqyg9zii5blc47hz2lfcfjz-ntfs-3g-2017.3.23"
      (propagated-inputs ())
      (search-paths ()))
     ("gparted"
      "0.28.1"
      "out"
      "/gnu/store/xxl12cc72fxzp9y35inbx17vkcvag8bs-gparted-0.28.1"
      (propagated-inputs ())
      (search-paths ()))
     ("parted"
      "3.2"
      "out"
      "/gnu/store/ylqnd6l728g7ynmnbcfw2r1xy3dj700z-parted-3.2"
      (propagated-inputs ())
      (search-paths ()))
     ("conkeror"
      "1.0.3"
      "out"
      "/gnu/store/xqsm1pxljx4miw4rakpnbcbhb7hvn1qz-conkeror-1.0.3"
      (propagated-inputs ())
      (search-paths ()))
     ("gcc"
      "7.2.0"
      "out"
      "/gnu/store/yd9kc56mm2ykxi4vv3205p0cc4s8vcwk-gcc-7.2.0"
      (propagated-inputs ())
      (search-paths
        (("C_INCLUDE_PATH" ("include") ":" directory #f)
         ("CPLUS_INCLUDE_PATH"
          ("include")
          ":"
          directory
          #f)
         ("LIBRARY_PATH" ("lib" "lib64") ":" directory #f))))
     ("make"
      "4.2.1"
      "out"
      "/gnu/store/s2xx79ck75rqnar3d45chdw4671ixc7c-make-4.2.1"
      (propagated-inputs ())
      (search-paths ()))
     ("cmus"
      "2.7.1"
      "out"
      "/gnu/store/1j5y54lanf0q09lc3m7sxi2k03jqx399-cmus-2.7.1"
      (propagated-inputs ())
      (search-paths ()))
     ("st"
      "0.7"
      "out"
      "/gnu/store/kvjs5sr6gnwlz9nm96gw9vcnwrn65h8l-st-0.7"
      (propagated-inputs ())
      (search-paths ()))
     ("lynx"
      "2.8.9dev.15"
      "out"
      "/gnu/store/ixnf218fa6cw1hnq2n2fbfkcvmi19jm8-lynx-2.8.9dev.15"
      (propagated-inputs ())
      (search-paths ()))
     ("slim"
      "1.3.6"
      "out"
      "/gnu/store/jkbpz3g9k9gqi315p6yci8wwx7lflknm-slim-1.3.6"
      (propagated-inputs ())
      (search-paths ()))
     ("gcc-toolchain"
      "7.2.0"
      "out"
      "/gnu/store/rxd7gsz1bci1chx5aq1pq57daxbi70d7-gcc-toolchain-7.2.0"
      (propagated-inputs ())
      (search-paths
        (("C_INCLUDE_PATH" ("include") ":" directory #f)
         ("CPLUS_INCLUDE_PATH"
          ("include")
          ":"
          directory
          #f)
         ("LIBRARY_PATH" ("lib" "lib64") ":" directory #f))))
     ("youtube-dl"
      "2017.08.18"
      "out"
      "/gnu/store/87i36xsj67h8i9qkvrv10w58qrgdb3g3-youtube-dl-2017.08.18"
      (propagated-inputs ())
      (search-paths ()))
     ("ghostscript"
      "9.14.0"
      "out"
      "/gnu/store/bnb1ydd51n7262i65dp98vdpd5ra0psr-ghostscript-9.14.0"
      (propagated-inputs ())
      (search-paths ()))
     ("xfontsel"
      "1.0.5"
      "out"
      "/gnu/store/gvmf123hxm597x4cp3n9qa91jljld431-xfontsel-1.0.5"
      (propagated-inputs ())
      (search-paths ()))
     ("mu"
      "0.9.18"
      "out"
      "/gnu/store/shlryirs2rbz0n63ixh1frhimi0gjfds-mu-0.9.18"
      (propagated-inputs ())
      (search-paths ()))
     ("procmail"
      "3.22"
      "out"
      "/gnu/store/bj3kqgdrancrkjjw59nscs2d45vfim8n-procmail-3.22"
      (propagated-inputs ())
      (search-paths ()))
     ("fetchmail"
      "6.3.26"
      "out"
      "/gnu/store/h2svgj2f5d9ql6xbiy8s4nrv6888bfa1-fetchmail-6.3.26"
      (propagated-inputs ())
      (search-paths ()))
     ("sshpass"
      "1.06"
      "out"
      "/gnu/store/9dxcfg9lpc5il59zicsn50r70nzm3y2c-sshpass-1.06"
      (propagated-inputs ())
      (search-paths ()))
     ("niebie-spoon"
      "0.0"
      "out"
      "/gnu/store/56rbjaz8xy0plyfjdqrwd88vrfpsj36w-niebie-spoon-0.0"
      (propagated-inputs ())
      (search-paths ()))
     ("postgresql"
      "9.6.4"
      "out"
      "/gnu/store/2sqs8ngbbpxln7bba63yazl79jy738lm-postgresql-9.6.4"
      (propagated-inputs ())
      (search-paths ()))
     ("curl"
      "7.55.0"
      "out"
      "/gnu/store/qz8kycs0bsl9q1r2kb8495p3q1i6fwal-curl-7.55.0"
      (propagated-inputs ())
      (search-paths ()))
     ("libreoffice"
      "5.3.5.2"
      "out"
      "/gnu/store/z64bzxp3ybcxxkq0l54hddg38g925s75-libreoffice-5.3.5.2"
      (propagated-inputs ())
      (search-paths ()))
     ("niebie-dwm"
      "0.0"
      "out"
      "/gnu/store/jws65g49mj90652bmn0r21104z7qa1yd-niebie-dwm-0.0"
      (propagated-inputs ())
      (search-paths ()))
     ("dosfstools"
      "4.1"
      "out"
      "/gnu/store/qm1dq0jr7xqq2rb2dyk4n2x36rm78nf4-dosfstools-4.1"
      (propagated-inputs ())
      (search-paths ()))
     ("swaks"
      "20170101.0"
      "out"
      "/gnu/store/994h7a8a6pnjydrw76n7j4h84w1vyinl-swaks-20170101.0"
      (propagated-inputs ())
      (search-paths ()))
     ("cvs"
      "1.12.13"
      "out"
      "/gnu/store/l6r62nad8c2zmz5d93z1g4cmwhlp4rwm-cvs-1.12.13"
      (propagated-inputs ())
      (search-paths ()))
     ("evince"
      "3.24.1"
      "out"
      "/gnu/store/37jwh0hzpw4qwch1imwa0wa2g3ii4lcv-evince-3.24.1"
      (propagated-inputs ())
      (search-paths ()))
     ("unzip"
      "6.0"
      "out"
      "/gnu/store/a4kajm2jg7i07xbkbrqarkrn2pb6d1bv-unzip-6.0"
      (propagated-inputs ())
      (search-paths ()))
     ("git"
      "2.14.1"
      "out"
      "/gnu/store/g7bj513yfla15j728m27rpmf6k8bfk9g-git-2.14.1"
      (propagated-inputs ())
      (search-paths
        (("GIT_SSL_CAINFO"
          ("etc/ssl/certs/ca-certificates.crt")
          #f
          regular
          #f)
         ("GIT_EXEC_PATH"
          ("libexec/git-core")
          #f
          directory
          #f))))
     ("xrdb"
      "1.1.0"
      "out"
      "/gnu/store/2qac6jqdcsv73nmmcxxszjln9zabhdxx-xrdb-1.1.0"
      (propagated-inputs ())
      (search-paths ()))
     ("ncurses"
      "6.0"
      "out"
      "/gnu/store/padj9dg2p0dl82fhyhp73z2nqs5pyfrv-ncurses-6.0"
      (propagated-inputs ())
      (search-paths
        (("TERMINFO_DIRS"
          ("share/terminfo")
          ":"
          directory
          #f))))
     ("openssh"
      "7.5p1"
      "out"
      "/gnu/store/896ww5ja6v4wx118ja8z8sdc62v4a5qv-openssh-7.5p1"
      (propagated-inputs ())
      (search-paths ()))
     ("lshw"
      "B.02.18"
      "out"
      "/gnu/store/fi7cd064fmbhlkclwm7mbp7kwhxyxrd1-lshw-B.02.18"
      (propagated-inputs ())
      (search-paths ()))
     ("pkg-config"
      "0.29.1"
      "out"
      "/gnu/store/98h5yp0l8mk2yijhcprrih68l44s44cx-pkg-config-0.29.1"
      (propagated-inputs ())
      (search-paths
        (("PKG_CONFIG_PATH"
          ("lib/pkgconfig"
           "lib64/pkgconfig"
           "share/pkgconfig")
          ":"
          directory
          #f))))
     ("alsa-lib"
      "1.1.3"
      "out"
      "/gnu/store/443r6hf4y7mfl5yg8099n1133s3zif2j-alsa-lib-1.1.3"
      (propagated-inputs ())
      (search-paths ()))
     ("binutils"
      "2.27"
      "out"
      "/gnu/store/qy42g3xqd7yk7spcr8wpxz98574j0q60-binutils-2.27"
      (propagated-inputs ())
      (search-paths ())))))
