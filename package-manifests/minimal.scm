(use-package-modules emacs ;duh
                     version-control ;git
                     ;;web
                     suckless ;st
                     admin ;htop
                     linux ;alsa-utils (alsamixer)
                     music ;cmus
                     curl ;duh
                     libreoffice ;duh
                     compression ;unzip
                     ssh ;duh
                     xorg ;xsetroot
                     )

(use-modules (niebie ;niebie-spoon, niebie-st
              ))

(packages->manifest
 (list emacs
       emacs-guix
       git
       (list git "gui")
       surf
       htop
       alsa-utils
       cmus
       curl
       libreoffice
       unzip
       openssh
       xsetroot
       niebie-spoon
       ;;st
       niebie-st
       ;;niebie-dwm ;; this isn't needed for user space.
       ))
