(use-package-modules emacs ;duh
                     version-control ;git
                     suckless ;st
                     ssh ;duh
                     dvtm ;duh
                     compression
                     )

(use-modules (niebie ;niebie-spoon
              ))

(packages->manifest
 (list emacs-no-x
       git
       dvtm
       st
       emacs-guix
       openssh
       unzip
       ))
